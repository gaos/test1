#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Mar  8 16:37:35 2020

@author: gabriel
diagonal creciente i=j, j=i
diagonal decreciente n-j, j
fila = i
columnas = j
"""

import random


# funcion encargada de revisar filas y columnas
def cruz(matriz, x, y):
    
    contador_filas = 0
    contador_columnas = 0
    
    for i in range(len(matriz)):
        for j in range(len(matriz)):
            if i==y:
                contador_filas = contador_filas + matriz[i][j]
            if j==x:
                contador_columnas = contador_columnas + matriz[i][j]
    return contador_columnas,contador_filas
                
#imprime una matriz
def imprime_matriz(matriz):
    
    for i in range(len(matriz)):
        for j in range(len(matriz)):
            print(matriz[i][j], end='')
        print('')
        
def comparador(columnas, filas, diagonal_c, diagonal_d):
    if(columnas.count(diagonal_c) == len(columnas) and diagonal_c == diagonal_d
       and filas.count(diagonal_c) == len(columnas)
        ):
        print("Es magico")
        return True
    else:
        print("No es magico")
        return False
    

def funcion_contadora(matriz):
    
    contador_filas = 0
    contador_columnas = 0
    contador_diagonal_c = 0
    contador_diagonal_d = 0
    tamano = len(matriz) -1
    contador_f_todas = []
    contador_c_todas = []
    
    for i in range(len(matriz)):
        for j in range(len(matriz)):
            
            if i==j:
                contador_diagonal_d = contador_diagonal_d + matriz[i][j]
                contador_columnas, contador_filas = cruz(matriz, j, i)
                contador_f_todas.append(contador_filas)
                contador_c_todas.append(contador_columnas)
                
            if(i == tamano - j):
                contador_diagonal_c = contador_diagonal_c + matriz[i][j]
    
    print("contador diagonal decreciente:", contador_diagonal_d,","
          ,"contador diagonal creciente:", contador_diagonal_c
          )
    for i in range(len(matriz)):
        print(contador_f_todas[i], " en la fila:", i)
        print(contador_c_todas[i], " en la columnas:", i)
    resultado = comparador(contador_f_todas, contador_c_todas, 
                           contador_diagonal_c, contador_diagonal_d
                            )
    return resultado

def hacer_matriz():
    
    try:
        matriz = []
        tamano = int(input('Ingrese el tamaño de la matriz:'))
        for i in range(tamano):
            matriz.append([0]*tamano)
        for i in range(tamano):
            for j in range(tamano):
                matriz[i][j]= random.randrange(2)
        return matriz
    except:
        print("Hubo un error")
    

def main():
    try:
        matriz = hacer_matriz()
        imprime_matriz(matriz)
        bandera = funcion_contadora(matriz)
        print(bandera)
    except:
        print("Ops, ocurrio un error")
    
if __name__=='__main__':
    main()